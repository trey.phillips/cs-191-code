import math, sys
import matplotlib.pyplot as plt
import numpy as np
import scipy.integrate as intg
from Fraction import *


def get_alpha(betas=None):
    """
    Calculates α as perscribed in the Paper by Pouly and Bournez
    @param A: [Numbers]
    @return: α
    """
    b_recips = [Fraction(n).reciprocal() for n in betas]
    return sum_fractions(b_recips).simplify()


def get_betas(A):
    """
    Calculates b_n for all values in a given array as in perscribed in the Paper by Pouly and Bournez
    @param A: [Numbers]
    @return: [b(n) | ∀n ∈ A]
    """
    betas = []
    for i in range(len(A)):
        betas.append(math.prod(A[: i + 1]))

    return betas


def get_sequence(fun, length):
    """
    Gets the n-length sequence of outputs of a given function.
    @param fun: Function which will be used to map
    @param length: Int desired length of sequence
    @return: [fun(n) | ∀n ∈ [2..length+2]]
    """
    return [fun(i) for i in range(2, length + 2)]


def get_period(alpha):
    """
    Calculates the period of f(t) based on a given alpha.
    @param alpha: α per Pouly and Bournez
    @return: String representing the radial period of f(t)
    """
    return alpha.denominator * 2 * math.pi


def n2(n):
    return n * 2


def ns(n):
    return n**2


def cf(n):
    return 3 - 2 ** -(n - 1)


def f(t, a):
    return (2 - math.cos(t) - math.cos(a * t)) ** -1


def g(t, a):
    return intg.quad(f, 1, t, (a))


def accept_sequence():
    return [
        int(n.strip())
        for n in input(
            "Input desired increasing sequence of integers greater than 2 (delimited by commas): "
        ).split(",")
    ]


def accept_alphas():
    return [
        Fraction(float(n.strip())).simplify()
        for n in str(input("Input desired alphas (delimited by commas): ")).split(",")
    ]


def accept_functions(funcs):
    alphas = []
    for func in funcs:
        seq = get_sequence(func, 25)
        betas = get_betas(seq)
        alpha = get_alpha(betas)
        alphas.append(alpha)

    return alphas


if __name__ == "__main__":
    mode = int(
        input(
            "Do you want to...\n\t1 - Input a sequence of integers\n\t2 - Input a sequence of alphas\n\t3 - See graph for predefined functions\nEnter Choice: "
        )
    )

    if mode == 1:
        sequence = accept_sequence()
        betas = get_betas(sequence)
        alphas = [get_alpha(betas)]
        print(alphas[0])
    elif mode == 2:
        alphas = accept_alphas()
    elif mode == 3:
        alphas = accept_functions([n2, ns, cf])

    fig, ax = plt.subplots(figsize=(8, 9))  # Initializes matplotlib plot
    x_max = min([100, math.floor(min([get_period(a) for a in alphas]) * 0.9)])
    xs = np.linspace(
        1, x_max, 200 * x_max
    )  # Sets the x range that will be passed to functions

    for a in alphas:
        # Defines f_t and g_t as partial applications of f and g
        f_t = lambda t: f(t, a.to_float())
        g_t = lambda t: g(t, a.to_float())[0]
        # Graphs f_t and g_t upon the matplotlib plot
        functions_to_graph = [
            (f_t, f"f(t) where alpha = {a.to_float()}"),
            (g_t, f"g(t) where alpha = {a.to_float()}"),
        ]

        for func, name in functions_to_graph:
            ys = [func(x) for x in xs]
            ax.plot(xs, ys, label=name)  # plot with automatically generated colors

    # Setting figure properties
    ax.set_title("f(t), and g(t)")
    ax.set_xlabel("Time (s)")
    ax.legend()
    fig.canvas.manager.set_window_title("Visualizing f(t) and g(t)")
    plt.show()
