This project uses Poetry for dependency management. For installation instructions see: https://python-poetry.org/docs/

Once poetry is installed, you can install all of the project's dependencies by typing "poetry install" into the 
terminal while in the git directory.
