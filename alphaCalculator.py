import math, random
from Fraction import *


def get_alpha(betas=None):
    """
    Calculates α as perscribed in the Paper by Pouly and Bournez
    @param A: [Numbers]
    @return: α
    """
    b_recips = [Fraction(n).reciprocal() for n in betas]
    return sum_fractions(b_recips).simplify()


def get_betas(A):
    """
    Calculates b_n for all values in a given array as in perscribed in the Paper by Pouly and Bournez
    @param A: [Numbers]
    @return: [b(n) | ∀n ∈ A]
    """
    betas = []
    for i in range(len(A)):
        betas.append(math.prod(A[: i + 1]))

    return betas

if __name__ == "__main__":
    mode = int(input("Random sequence (1) or Custom Sequence (2): "))
    if mode == 1:
        sequence = sorted(random.sample(range(2, 50), 6))
    elif mode == 2:
        sequence = input("Input desired increasing sequence of integers (each>=2): ")
        sequence = [
            int(n.strip()) for n in sequence.split("," if "," in sequence else " ")
        ]

    betas = get_betas(sequence)
    alphas = [get_alpha(betas[:n]) for n in range(1, len(betas) + 1)]
    tois = ["2p" + (str(b) if b > 1 else "") for b in betas][:-1]
    alphas_str = str([str(alpha) for alpha in alphas]).replace("'", "")
    tois_str = str(tois).replace("'", "")
    print(
        f"sequence = {sequence}\n𝛼_n = {alphas_str}\n𝛼_{len(sequence)} = {alphas[-1]}\nTimes of interest = {tois_str}"
    )
