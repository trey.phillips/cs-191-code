class Fraction:
    def __init__(self, numerator, denominator=None):
        """
        Initializes a fraction with a given numerator and denominator
        @param numerator: Number signifying the numerator of a fraction
        @param denominator: Number representing the denominator of a fraction, defaults to 1
        """
        if type(numerator) == float and numerator % 1:
            num_int, num_dec = str(numerator).split(".")
            self.denominator = 10 ** len(num_dec)
            self.numerator = self.denominator * int(num_int) + int(num_dec)
        else:
            self.numerator = numerator if type(numerator) == int else int(numerator)
            self.denominator = denominator if denominator else 1

    def to_float(self):
        # Returns float representation of this fraction
        return self.numerator / self.denominator

    def reciprocal(self):
        # Returns the reciprocal of this Fraction.
        return Fraction(self.denominator, self.numerator)

    def gcf(self):
        # Returns the greatest common factor of this Fraction's numerator and denominator.
        x = self.numerator
        y = self.denominator
        while y:
            x, y = y, x % y
        return x

    def simplify(self):
        # Returns the most simplified form of this Fraction
        gcf = self.gcf()
        return Fraction(self.numerator // gcf, self.denominator // gcf) if gcf else self

    # String Representations
    __str__ = lambda self: f"{self.numerator:.0f} / {self.denominator:.0f}"
    __repr__ = lambda self: f"Fraction: {self.numerator:.0f} / {self.denominator:.0f}"

    # Arithmetic Operators
    __add__ = (
        lambda self, other: Fraction(
            self.numerator * other.denominator + other.numerator * self.denominator,
            self.denominator * other.denominator,
        )
        if type(other) == Fraction
        else Fraction(self.numerator + other * self.denominator, self.denominator)
    )
    __sub__ = lambda self, other: Fraction(
        self.numerator * other.denominator - other.numerator * self.denominator,
        self.denominator * other.denominator,
    )
    __mul__ = lambda self, other: Fraction(
        self.numerator * other.numerator, self.denominator * other.denominator
    )
    __truediv__ = lambda self, other: self.__mul__(other.reciprocal())
    __pow__ = (
        lambda self, n: Fraction(self.denominator ** (-n), self.numerator ** (-n))
        if n < 0
        else Fraction(self.numerator**n, self.denominator**n)
    )

    def __mod__(self, n):
        resulting_numerator = self.numerator
        while resulting_numerator > self.denominator * n:
            resulting_numerator -= self.denominator * n
        return Fraction(resulting_numerator, self.denominator)

    # Boolean Operators
    __eq__ = (
        lambda self, other: self.numerator / self.denominator
        == other.numerator / other.denominator
    )
    __lt__ = (
        lambda self, other: self.numerator / self.denominator
        < other.numerator / other.denominator
    )
    __le__ = (
        lambda self, other: self.numerator / self.denominator
        <= other.numerator / other.denominator
    )
    __gt__ = (
        lambda self, other: self.numerator / self.denominator
        > other.numerator / other.denominator
    )
    __ge__ = (
        lambda self, other: self.numerator / self.denominator
        >= other.numerator / other.denominator
    )
    __ne__ = (
        lambda self, other: self.numerator / self.denominator
        != other.numerator / other.denominator
    )


def sum_fractions(arr, total=0):
    """
    Return the sum of all fractions in an iterable of fractions.
    @param arr: [Fraction]
    @param total: Fraction equaling the running total of fractions in arr
    @return: Simplified Fraction equaling the total of all fractions in arr
    """
    if not total:
        total = arr.pop(0)
    if len(arr) == 0:
        return total.simplify()
    else:
        next_frac = arr.pop(0)
        return sum_fractions(arr, total + next_frac)
