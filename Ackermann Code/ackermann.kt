fun A(m: Int, n: Int): Int {
	if (m == 0) {
		return n + 1
	} else if (n == 0) {
		return A(m - 1, 1)
	} else {
		return A(m - 1, A(m, n - 1))
	}
}

fun main() {
	println(A(3, 3))
}