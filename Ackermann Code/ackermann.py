import time
from subprocess import call


def A(m, n):
    # 	return n + 1 if m == 0 else A(m - 1, 1) if n == 0 else A(m - 1, A(m, n - 1))
    # global recursionCount
    if m == 0:
        return n + 1

    if n == 0:
        # recursionCount += 1
        return A(m - 1, 1)

    # recursionCount += 1
    return A(m - 1, A(m, n - 1))


if __name__ == "__main__":
    # Test Inputs and Results from https://www.google.com/url?sa=i&url=https%3A%2F%2Ftex.stackexchange.com%2Fquestions%2F584112%2Fackermann-function&psig=AOvVaw3ErzEn2YPq1Zb_3UoqcjQS&ust=1674597539275000&source=images&cd=vfe&ved=0CA4QjRxqFwoTCLC4zY3Y3vwCFQAAAAAdAAAAABAV

    test_inputs = [
        (0, 0),
        (1, 0),
        (2, 0),
        (0, 1),
        (0, 2),
        (1, 1),
        (2, 2),
        (2, 3),
        (3, 3),
        (3, 4),
    ]
    test_outputs = [1, 2, 3, 2, 3, 3, 7, 9, 61, 125]

    # Checks all inputs for correct output
    correct_count = 0
    for i, input in enumerate(test_inputs):
        recursionCount = 1
        result = A(input[0], input[1])
        print(
            f"A({input[0]}, {input[1]}) => {result}\tNumber of times A was called: {recursionCount}"
        )
        correct_count += 1 if test_outputs[i] == result else 0

    # Display number correct
    print(f"{correct_count} out of {len(test_outputs)} correct.")
