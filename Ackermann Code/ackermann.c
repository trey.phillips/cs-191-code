#include <stdio.h>

int A(int m, int n) {
	
	if (m == 0) return n + 1;
	
	if (n == 0) return A(m-1, 1);
	
	return A(m-1, A(m, n-1));
}

int main(int argc, char** argv) {
	// Enter desired value for m
	int m = 4;
	
	// Enter desired value for n
	int n = 1;
	
	// Prints A(m, n) to the console
	printf("%d\n", A(m, n));
}